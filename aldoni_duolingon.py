#! /usr/bin/python

import json
import sqlite3
import urllib.request as urllib2

def TestoDeDatumojEnDuolingo(lingvo, parolanto, sumo):
  #Cxu la datumo jam ekzistas en la datumbazo?
  rik =  c.execute('''SELECT sumo
                             FROM duolingo
                             WHERE lingvo='{}' AND parolanto='{}'
                             ORDER BY dato
                             DESC LIMIT 1'''.format(lingvo,parolanto)).fetchall()
  if len(rik) == 0:
      return False
  kontrolilo = (rik[0][0] == int(sumo)) # kontrolilo estas 'True' Se la lasta datumo egalas la novan

  return kontrolilo


# Retpagxon
url = 'https://incubator.duolingo.com/api/1/courses/list'
enhavo = urllib2.urlopen(url)

# Enhavo -> Objekto
objekto = json.load(enhavo)

# Sqlite
konekto = sqlite3.connect('data.db')
c = konekto.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS duolingo
             (id integer primary key autoincrement, dato timestamp DEFAULT CURRENT_TIMESTAMP,lingvo text, parolanto text, sumo integer)''')

# sintakse analizi
direktoj = objekto['directions']

valoroj = {}

for p in direktoj:
    # Ne aldonu unuan fazon
    if p['phase'] == 1:
        continue

    lingvo = p['learning_language_id']
    parolanto = p['from_language_id']
    sumo = p['learner_count']['num_learners']

    
    if TestoDeDatumojEnDuolingo(lingvo, parolanto, sumo) == False :
        c.execute('''INSERT INTO duolingo (dato, lingvo, parolanto, sumo) SELECT CURRENT_TIMESTAMP,?,?,?''', (lingvo, parolanto, sumo))

        print('Novaj datumoj de\t{}->{}\t\taldonas en la datumbazo.'.format(lingvo, parolanto))
    else :
        print('Novaj datumoj de\t{}->{}\t\tjam estas en la datumbazo.'.format(lingvo, parolanto))
        

konekto.commit()
konekto.close()

