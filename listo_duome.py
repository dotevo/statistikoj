#! /usr/bin/python
import sqlite3
import time
import datetime
import sys
from listo import showData
from pprint import pprint

d_1=sys.argv[1]
d_2=sys.argv[2]
d_3='l'

if len(sys.argv) >= 3:
    d_3 = sys.argv[3]

konekto = sqlite3.connect('data.db')
c = konekto.cursor()
sql = '''SELECT date(dato),lingvo,
    avg(lernantoj) as lernantoj,
    avg(arboj) as arboj,
    avg(niveloP10) as niveloP10,
    avg(orajStrigoj) as orajStrigoj,
    avg(orajArboj) as orajArboj
  FROM duome WHERE date(dato)=?'''

sql+= """ GROUP BY date(dato), lingvo ORDER BY dato, lingvo"""

valoroj = {}
for rikordo in c.execute(sql,[d_1]):
    valoroj[rikordo[1]] = {
      "mnl" : rikordo[2], "nl": 0, "dl": 0 - rikordo[2],
      "mna" : rikordo[3], "na": 0, "da": 0 - rikordo[3],
      "mnn" : rikordo[4], "nn": 0, "dn": 0 - rikordo[4],
      "mnos" : rikordo[5], "nos": 0, "dos": 0 - rikordo[5],
      "mnoa" : rikordo[6], "noa": 0, "doa": 0 - rikordo[6],
    }

for rikordo in c.execute(sql,[d_2]):
    if rikordo[1] not in valoroj:
        valoroj[rikordo[1]] = {
            "mnl" : 0,
            "mna" : 0,
            "mnn" : 0,
            "mnos" : 0,
            "mnoa" : 0
        }
    valoroj[rikordo[1]]["nl"] = rikordo[2]
    valoroj[rikordo[1]]["na"] = rikordo[3]
    valoroj[rikordo[1]]["nn"] = rikordo[4]
    valoroj[rikordo[1]]["nos"] = rikordo[5]
    valoroj[rikordo[1]]["noa"] = rikordo[6]

    valoroj[rikordo[1]]["dl"] = rikordo[2] - valoroj[rikordo[1]]["mnl"]
    valoroj[rikordo[1]]["da"] = rikordo[3] - valoroj[rikordo[1]]["mna"]
    valoroj[rikordo[1]]["dn"] = rikordo[4] - valoroj[rikordo[1]]["mnn"]
    valoroj[rikordo[1]]["dos"] = rikordo[5] - valoroj[rikordo[1]]["mnos"]
    valoroj[rikordo[1]]["doa"] = rikordo[6] - valoroj[rikordo[1]]["mnoa"]

    #valoroj[rikordo[1]]["c"] = 100 * float(valoroj[rikordo[1]]["nl"] - valoroj[rikordo[1]]["mnl"]) / (valoroj[rikordo[1]]["mnl"]+1)

dictlist = []
for sxlosilo, valoro in valoroj.iteritems():
    valoro["lingvo"] = sxlosilo
    temp = [sxlosilo, valoro]
    dictlist.append(temp)


print("------ LERNANTOJ -----")
showData(dictlist, ['lingvo', 'nl', 'na', 'nn', 'nos', 'noa'], ['lingvo', 'lernantoj', 'arbaroj', 'nivelo','oraj strigoj','oraj arbaroj'], 'n' + d_3)

print("------ DIFERENCO -----")
showData(dictlist, ['lingvo', 'dl', 'da', 'dn', 'dos', 'doa'], ['lingvo', 'lernantoj', 'arbaroj', 'nivelo','oraj strigoj','oraj arbaroj'], 'd' + d_3)

konekto.close()
