#! /usr/bin/python

import os
import sqlite3
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdat
import numpy as np

def kreiduome(lnvo, param):

    try :
        os.makedirs(u'duome\{}'.format(lnvo))
    except :
        pass

    datumoj = c.execute('''SELECT dato,
                                  {}
                            FROM duome
                            WHERE lingvo = '{}'
                            ORDER BY dato '''.format(param, lnvo)).fetchall()

    x=np.linspace(0,len(datumoj),len(datumoj))
    x = []
    y = []
    for dato, param_d in datumoj :
        x.append( mdat.num2date( datetime.datetime(*map(int,dato.replace('-',' ').replace(':',' ').split(" "))).toordinal()) )
        y.append(param_d)

    plt.plot(x, y)
    plt.xlabel('Dato')
    plt.ylabel('{}'.format(param))
    plt.title('Statistikoj : duome-{}'.format(lnvo))
    plt.grid(True)
    plt.gcf().autofmt_xdate(rotation=90)
    plt.savefig('duome\{}\{}.png'.format(lnvo,param))
    plt.clf()
    plt.close()

def kreiduolingo(lnvo):

    try :
        os.makedirs(u'duolingo')
    except :
        pass

    hdl = []
    lingvojlisto = []
    curslingv = c.execute('''SELECT parolanto
                             FROM duolingo
                             WHERE lingvo = '{}'
                             GROUP BY parolanto
                             ORDER BY parolanto'''.format(lnvo)).fetchall()
    lingvlisto = []
    for p in curslingv :
        print u'\t{}'.format(p[0])
        datumoj = c.execute('''SELECT dato,
                                      sumo
                                FROM duolingo
                                WHERE lingvo = '{}' AND parolanto = '{}'
                                ORDER BY dato'''.format(lnvo, p[0])).fetchall()

        x=np.linspace(0,len(datumoj),len(datumoj))
        x = []
        y = []
        for dato, param_d in datumoj :
            x.append( mdat.num2date( datetime.datetime(*map(int,dato.replace('-',' ').replace(':',' ').split(" "))).toordinal()) )
            y.append(param_d)

        plt.plot(x, y)
        lingvlisto.append(p[0])

    plt.legend(lingvlisto, loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel('Dato')
    plt.ylabel('sumo')
    plt.title('Statistikoj : duolingo-{}'.format(lnvo))
    plt.grid(True)
    plt.gcf().autofmt_xdate(rotation=90)
    plt.savefig('duolingo\{}_sumo.png'.format(lnvo), bbox_inches = "tight")
    plt.clf()
    plt.close()


c = None

#SQL
konekto = sqlite3.connect('data.db')
c = konekto.cursor()




# duome
lingvoj = c.execute('''SELECT lingvo FROM duome GROUP BY lingvo''').fetchall()

kolumnlisto = []
curstable = c.execute('''PRAGMA table_info(duome)''')
for p in curstable :
    print p
    kolumnlisto.append(p[1])
kolumnlisto.remove('id')
kolumnlisto.remove('dato')
kolumnlisto.remove('lingvo')

print u'kolumnlisto :\n{}'.format(kolumnlisto)

for p in lingvoj:
    print u'Lingvo : {}'.format(p[0])
    for kol in kolumnlisto :
        kreiduome(p[0], kol)


# duolingo
lingvoj = c.execute('''SELECT lingvo FROM duolingo GROUP BY lingvo''').fetchall()

for p in lingvoj:
    print u'Lingvo : {}'.format(p[0])
    kreiduolingo(p[0])



konekto.close()
