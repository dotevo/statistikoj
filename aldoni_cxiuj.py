#!/usr/bin/env python
# -*- coding: utf-8 -*

import ConfigParser     # Por la agordoj de la skriptoj

import sqlite3          # Por la datumbazo
import urllib2          # Por elsxuti la datumoj
import re               # Por filtri la datumoj sur la retpagxoj
import json             # Por formdatumoj de Duolingo

debug = False

agordoj = ConfigParser.RawConfigParser() # Agorda kreado
agordoj.read('.aldonaj_agordoj') # Oni legas la dosieron de agordoj

##agordlisto = {
##    'Duolingo_tasknomo' : agordoj.get('Duolingo'    ,'Tasknomo'),
##    'Duome_tasknomo'    : agordoj.get('Duome'       ,'Tasknomo'),
##    'Cxiuj_taskoj'      : { 'tasknomo' : agordoj.get('Cxiuj_taskoj','Tasknomo'),
##                            'aktivado' : agordoj.get('Cxiuj_taskoj','aktivado')},
##    'Skripta_agordoj'   : {'Tempo_antaux_reelsxuti' : agordoj.get('Agordoj' ,'Tempo_antaux_reelsxuti'),
##                           'Python_win'             : agordoj.get('Agordoj' ,'Python_win')}
##}

##print agordlisto
##for part in agordlisto :
##    print part
##    try :
##        if len(agordlisto[part].keys())>1:
##            for par in agordlisto[part]:
##                print u'\t' + par
##    except :
##        pass

###skribatesto
##agordoj.set('Duolingo'    ,'ID', '24556146a4b5c55f464e64c64d646b4e')
##
##with open('.aldonaj_agordoj', 'wb') as agordadosiero:
##    agordoj.write(agordadosiero) # Oni skribas la dosieron de agordoj


def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, ' ', raw_html)
  return cleantext


def TestoDeDatumojEnDuome(cursor, lingvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees):
  #Cxu la datumo jam ekzistas en la datumbazo?
  lasta_datumo =  cursor.execute('''SELECT  lernantoj, arboj, niveloP10, orajStrigoj, orajArboj FROM duome WHERE lingvo='{}' ORDER BY dato DESC LIMIT 1'''.format(lingvo)).fetchall()[0]

  kontrolilo = False # kontrolilo estas 'True' Se la lasta datumo estas simila al nova

  if len(lasta_datumo)==0 : # nova lingvo
    return kontrolilo

  if debug == True :
      print u'Komparo por %s:'%lingvo
      print u'Lasta\tNova'
      print u'{}\t{}\tlernantoj'.format(lasta_datumo[0],nbr_students)
      print u'{}\t{}\tArboj'.format(lasta_datumo[1],nbr_Trees)
      print u'{}\t{}\t@ Nivelo 10+'.format(lasta_datumo[2],nbr_level_sup10)
      print u'{}\t{}\tOraj Strigoj'.format(lasta_datumo[3],nbr_golden_owl)
      print u'{}\t{}\tOraj Arboj'.format(lasta_datumo[4],nbr_golden_trees)
      print '-'*20

  # (lernantoj, arboj, niveloP10, orajStrigoj, orajArboj)
  # (    0    ,   1  ,     2   ,      3      ,      4   )
  if (lasta_datumo[0] == int(nbr_students)) and \
     (lasta_datumo[1] == int(nbr_Trees)) and \
     (lasta_datumo[2] == int(nbr_level_sup10)) and \
     (lasta_datumo[3] == int(nbr_golden_owl)) and \
     (lasta_datumo[4] == int(nbr_golden_trees)) :
      kontrolilo = True

  return kontrolilo

def TestoDeDatumojEnDuolingo(cursor, lingvo, parolanto, sumo):
  #Cxu la datumo jam ekzistas en la datumbazo?
  lasta_sumo =  cursor.execute('''SELECT sumo
                                 FROM duolingo
                                 WHERE lingvo='{}' AND parolanto='{}'
                                 ORDER BY dato
                                 DESC LIMIT 1'''.format(lingvo,parolanto)).fetchall()[0][0]
  kontrolilo = (lasta_sumo == int(sumo)) # kontrolilo estas 'True' Se la lasta datumo egalas la novan

  return kontrolilo


def elsxuti(Datumbazo_cursor, lnvo):
    url = 'https://duome.eu/' + lnvo
    data = urllib2.urlopen(url)

    text = data.read()
    text = re.findall(ur'Statistics([\w\W]*)<\/ul><\/div>', text, re.MULTILINE)[0]
    text = re.findall(ur'<ul>([\w\W]*)<\/ul>', text, re.MULTILINE)[0]

    # La filtroj
    regex_s = ur'(\d+)\s+Students'
    regex_t = ur'(\d+)\s+Trees'
    regex_l = ur'(\d+)\s+@ L10+'
    regex_go = ur'(\d+)\s+Golden Owls'
    regex_gt = ur'(\d+)\s+Golden Trees'

    # filtradoj
    nbr_students = re.findall(regex_s, text, re.MULTILINE)[0]
    nbr_Trees = re.findall(regex_t, text, re.MULTILINE)[0]
    nbr_level_sup10 = re.findall(regex_l, text, re.MULTILINE)[0]
    nbr_golden_owl = re.findall(regex_go, text, re.MULTILINE)[0]
    nbr_golden_trees = re.findall(regex_gt, text, re.MULTILINE)[0]

    if debug == True :
      print '*'*20
      print u'Novaj statistikoj por %s:'%lnvo
      print u'{} lernantoj'.format(nbr_students)
      print u'{} Arboj'.format(nbr_Trees)
      print u'{} @ Nivelo 10+'.format(nbr_level_sup10)
      print u'{} Oraj Strigoj'.format(nbr_golden_owl)
      print u'{} Oraj Arboj'.format(nbr_golden_trees)


    if TestoDeDatumojEnDuome(Datumbazo_cursor, lnvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees) == False :
      Datumbazo_cursor.execute('''INSERT INTO duome (dato, lingvo, lernantoj, arboj, niveloP10, orajStrigoj, orajArboj) \
                 SELECT CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?''', (lnvo, nbr_students, nbr_Trees, nbr_level_sup10, nbr_golden_owl, nbr_golden_trees))
      print u'Novaj datumoj de {} aldonas en la datumbazo.'.format(lnvo)
    else :
      print u'Novaj datumoj de {} jam estas en la datumbazo.'.format(lnvo)
      # Fari poste : aldoni en dosiero kiu oni devos relsxuti cxi tiu lingvo
      ## Refari la elsxuto en cxiu unu horo (por lingvoj kiuj estas en la reelsxuta dosiero) gxis la elsxuto estas nova
      ### Se la datumo estas nova, do forigi la lingvo de la dosiero kaj cron
      ### (Por schtasks, krei taskon kiu mem detruas post esti farita)



c = None
#SQL
konekto = sqlite3.connect('data.db')
c = konekto.cursor()

#####################################################################
print u'Komencas kun Duome :'
# Retpagxon
Duome_cxefapagxo = urllib2.urlopen('https://duome.eu/')

lingvoj = None
for l in Duome_cxefapagxo.read().split("\n"):
    if "Golden Owl" not in l:
        continue
    nehtml = cleanhtml(l)
    lingvoj = re.findall("([a-z]+)\s*[0-9]+",nehtml)
    if debug == True :
      print(lingvoj)
      print u'~'*20

    for p in lingvoj:
        elsxuti(c,p)

######################################################################
print u'Post kun Duolingo :'
# Retpagxon
url = 'https://incubator.duolingo.com/api/1/courses/list'
enhavo = urllib2.urlopen(url)

# Enhavo -> Objekto
objekto = json.load(enhavo)

# sintakse analizi
direktoj = objekto['directions']

valoroj = {}
for p in direktoj:
    # Ne aldonu unuan fazon
    if p['phase'] == 1:
        continue

    lingvo = p['learning_language_id']
    parolanto = p['from_language_id']
    sumo = p['learner_count']['num_learners']

    if TestoDeDatumojEnDuolingo(c, lingvo, parolanto, sumo) == False :
        c.execute('''INSERT INTO duolingo (dato, lingvo, parolanto, sumo) SELECT CURRENT_TIMESTAMP,?,?,?''', (lingvo, parolanto, sumo))

        print u'Novaj datumoj de\t{}->{}\t\taldonas en la datumbazo.'.format(lingvo, parolanto)
    else :
        print u'Novaj datumoj de\t{}->{}\t\tjam estas en la datumbazo.'.format(lingvo, parolanto)
      # Fari poste : aldoni en dosiero kiu oni devos relsxuti cxi tiu lingvo/parolanto
      ## Refari la elsxuto en cxiu unu horo (por lingvoj kiuj estas en la reelsxuta dosiero) gxis la elsxuto estas nova
      ### Se la datumo estas nova, do forigi la lingvo de la dosiero kaj cron
      ### (Por schtasks, krei taskon kiu mem detruas post esti farita)


##################################################
konekto.commit()
konekto.close()
