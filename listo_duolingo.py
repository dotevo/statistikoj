#! /usr/bin/python
import sqlite3
import time
import datetime
import sys
from listo import showData
from pprint import pprint
import argparse

def kreiArgumentojn():
    parser = argparse.ArgumentParser(description='Markdown listo')
    print parser
    parser.add_argument('de', nargs=1,
                        help='de dato')
    parser.add_argument('gxis', nargs=1,
                        help='gxis dato')
    parser.add_argument('--lingvo', nargs=1,
                        help='lingvo de la kurso')
    parser.add_argument('--instruita', nargs=1,
                        help='instruita lingvo')
    parser.add_argument('--grupigi', nargs=1, help='I - instruita lingvo, L - lingvo de la kurso')
    args = parser.parse_args()
    print args

    return args

INDEKSO_DATO = 0
INDEKSO_LINGVO = 1
INDEKSO_PAROLANTO = 2
INDEKSO_SUMO = 3

def sqlExec(c, sql, sqlargoj, valoroj, argoj, diferenco=False):
    for rikordo in c.execute(sql, sqlargoj):
        nomo = ""
        if argoj.grupigi == None or argoj.grupigi[0] == 'I':
            nomo += rikordo[INDEKSO_LINGVO]
        nomo += ":"
        if argoj.grupigi == None or argoj.grupigi[0] == 'L':
            nomo += rikordo[INDEKSO_PAROLANTO]

        if nomo not in valoroj:
            valoroj[nomo] = {"malnova":0, "nova": 0, "diferenco": 0}

        if diferenco:
            valoroj[nomo]["nova"] = rikordo[INDEKSO_SUMO]
            valoroj[nomo]["diferenco"] = rikordo[INDEKSO_SUMO] - valoroj[nomo]["malnova"]
            valoroj[nomo]["c"] = 100 * float(valoroj[nomo]["nova"] - valoroj[nomo]["malnova"]) / (valoroj[nomo]["malnova"]+1)
        else:
            valoroj[nomo] = {"malnova" : rikordo[INDEKSO_SUMO], "nova": 0, "diferenco": 0 - rikordo[INDEKSO_SUMO]}

def cxefa():
    argoj = kreiArgumentojn()
    #sqlite
    konekto = sqlite3.connect('data.db')
    c = konekto.cursor()

    #informpeto
    sqlargoj_1 = [argoj.de[0]]
    sqlargoj_2 = [argoj.gxis[0]]
    sql = """SELECT dato, lingvo, parolanto, sum(sumo) as sumo FROM duolingo WHERE date(dato)=?"""
    if argoj.instruita != None:
        sql += " AND lingvo = ?"
        sqlargoj_1.append(argoj.instruita[0])
        sqlargoj_2.append(argoj.instruita[0])

    if argoj.lingvo != None:
        sql += " AND parolanto = ?"
        sqlargoj_1.append(argoj.lingvo[0])
        sqlargoj_2.append(argoj.lingvo[0])

    sql+= """ GROUP BY dato """
    grupigi_I = 0
    if argoj.grupigi == None or argoj.grupigi[0] == 'I':
        sql+= """, lingvo """
    if argoj.grupigi == None or argoj.grupigi[0] == 'L':
        sql+= """, parolanto """
    sql+= """ ORDER BY dato, lingvo, parolanto"""

    valoroj = {}
    print sql
    print sqlargoj_1
    print sqlargoj_2
    sqlExec(c, sql, sqlargoj_1, valoroj, argoj)
    sqlExec(c, sql, sqlargoj_2, valoroj, argoj, True)

    dictlist = []
    for sxlosilo, valoro in valoroj.iteritems():
        sxlosilo = sxlosilo.split(":")
        valoro["instruita"] = sxlosilo[0]
        valoro["lingvo"] = sxlosilo[1]
        temp = [sxlosilo[0], valoro]
        dictlist.append(temp)


    print("------ UZANTOJ -----")
    showData(dictlist, ['instruita', 'lingvo', 'nova'], ['Instruita', 'Lingvo' ,'Nova'], "nova")

    print("------ DIFERENCO -----")
    showData(dictlist, ['instruita', 'lingvo', 'diferenco'], ['Instruita', 'Lingvo' ,'Diferenco'], "diferenco")

    konekto.close()

if __name__ == "__main__":

    if len(sys.argv)==1:
        #sys.argv = u'./listo_duolingo.py 2019-01-01 2019-03-01 --instruita eo --grupigi L'.split(u' ')
        sys.argv = u'./listo_duolingo.py 2018-09-25 2019-04-04 --instruita eo --grupigi I'.split(u' ')
    
    cxefa()
