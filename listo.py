#TODO: Esperantigi

def showData(data ,col, titles, sort):
    #create format
    f = '|{a1:>3} |'
    headerspilt = '|--|'
    header = '| V | '
    if isinstance(col, list):
        arr = col
        col = arr[0]
        n = 0
        for t in arr:
             f += ' {a' + str(n + 3) + ':>10}|'
             headerspilt += '--|'
             header += titles[n] + ' |'
             n += 1
    else:
        f += ' {a3:>10}|'
        arr = [col]
        headerspilt += '--|'
        header += titles + ' |'
    t = sorted(data, key=lambda dct: dct[1][sort], reverse=True)
    i = 0
    eo = -1

    print(header)
    print(headerspilt)
    for p in t:
        i+=1
        if p[0] == 'eo' and eo == -1:
            eo = i

        mapping= {'a1':i}
        n = 2
        for zz in arr:
            n += 1
            mapping['a' + str(n)] = p[1][zz]
        print(f.format(**mapping))
    print('Eo:  %d' % eo)
