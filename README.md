Skriptoj por elŝuti kelkajn informojn el la reto pri Esperanto

Ili aldonas datumojn al SQLite datumilo (data.db)

Pli:
https://gitlab.com/dotevo/statistikoj/wikis/Home


Bezonita:
1. sudo apt install sqlite3
2. pip3 install matplotlib

Kiel komenci:

1. git clone git@gitlab.com:dotevo/statistikoj.git
2. cd statistikoj
3. git clone git@gitlab.com:dotevo/statistikoj.git -b sqlite db
4. cd db
5. ./krei.sh
6. ../
7. sqlite3 data.db ".read db/db.sql"

